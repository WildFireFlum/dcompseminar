import plotly.graph_objs as go
import itertools
import networkx as nx
import matplotlib.pyplot as plt
from collections import defaultdict
import random
import colorsys
from networkx.drawing.nx_agraph import graphviz_layout

SEED = 1337
DEFAULT_EDGE_COLOR = 'gray'


def create_graph():
    # Creating a Graph
    G = nx.Graph()  # Right now G is empty

    # Add a node
    G.add_node(1)
    G.add_node(5)
    G.add_node(6)
    G.add_node(7)
    G.add_node(8)
    G.add_node(9)
    G.add_node(10)
    G.add_nodes_from([2, 3])  # You can also add a list of nodes by passing a list argument

    # Add edges
    # G.add_edge(1,2)

    e = (2, 3)
    G.add_edge(*e, color='r')  # * unpacks the tuple
    G.add_edges_from(
        [(1, 1), (1, 2), (1, 3), (1, 5), (7, 8), (7, 9), (9, 10)])  # Just like nodes we can add edges from a list

    G = nx.gnm_random_graph(11, 25, seed=SEED)

    return G


def plot_graph_to_file(G, file_path, g_layout, vertex_colors=None, edge_colors=DEFAULT_EDGE_COLOR):
    global SHOULD_PLOT
    if not SHOULD_PLOT:
        return None

    print 'Plotting graph to file {}'.format(file_path)
    f = plt.figure()
    options = {
        'node_color': 'skyblue' if not vertex_colors else [vertex_colors[val] for val in G.nodes.keys()],
        'edge_color': edge_colors,
        'cmap': plt.cm.RdYlGn,
        'linewidths': 4,
        'alpha': 0.95,
        #'font_weight': "bold",
        'font_size': "20",
        'edge_cmap': plt.cm.RdYlGn,
        'node_size': 1500,
        'width': 3.0,
        'arrows': True,
        'arrowstyle': '-|>',
        'arrowsize': 12,
    }

    plt.axis('off')
    nx.draw_networkx(G, pos=g_layout, ax=f.add_subplot(111), **options)
    f.set_facecolor("#ebe7dd")
    f.savefig(file_path, facecolor=f.get_facecolor())
    print 'Finished Plotting graph to file {}'.format(file_path)


def plot_match_graph(G, matching, file_path, g_layout):
    edge_colors = []
    for x, y in G.edges():
        color = 'red' if (x, y) in matching or (y, x) in matching else DEFAULT_EDGE_COLOR
        edge_colors.append(color)
    plot_graph_to_file(G, file_path, g_layout, edge_colors=edge_colors)


def plot_edge_coloring_graph(G, edge_coloring, file_path, g_layout):
    edge_colors = []
    for x, y in G.edges():
        color = edge_coloring[(x, y)] if (x, y) in edge_coloring else edge_coloring[(y, x)]
        edge_colors.append(color)
    plot_graph_to_file(G, file_path, g_layout, edge_colors=edge_colors)


def decompose_graph(G):
    print 'Computing forest decomposition of graph'
    ranks_to_edges = defaultdict(list)
    edges_proposals = dict()
    for node in G.nodes.keys():
        for proposal, neighbor in enumerate(G.neighbors(node)):
            max_id = max(node, neighbor)
            min_id = min(node, neighbor)
            if node == max_id:
                edges_proposals[(max_id, min_id)] = proposal
                print 'Added proposal {} for edge {}'.format(proposal, (max_id, min_id))

    for edge, rank in edges_proposals.items():
        ranks_to_edges[rank].append(edge)

    ranks_to_forests = dict()
    for rank in ranks_to_edges.keys():
        G = nx.DiGraph()
        for edge in ranks_to_edges[rank]:
            min_id = min(edge)
            max_id = max(edge)
            G.add_node(min_id)
            G.add_node(max_id)
            G.add_edge(min_id, max_id)
        ranks_to_forests[rank] = G

    return ranks_to_forests


def welsh_color_nodes(G):
    color_map = {}
    # Consider nodes in descending degree
    for node in sorted(G.nodes.keys(), reverse=True):
        neighbor_colors = set(color_map.get(neigh) for neigh in G.neighbors(node))
        color_map[node] = next(color for color in xrange(len(G)) if color not in neighbor_colors)
    return color_map


def get_all_colors(forest_colorings):
    return set(reduce(lambda x, y: x + y, [coloring.values() for coloring in forest_colorings], []))


def get_first_available_edge_color(edge, edges_colors, max_degree):
    relevant_edges = [colored_edge for colored_edge in edges_colors.keys()
                      if edge[0] in colored_edge or edge[1] in colored_edge]
    used_colors = sorted([edges_colors[colored_edge] for colored_edge in relevant_edges])
    return min([color for color in xrange((2 * max_degree) - 1) if color not in used_colors])


def match_graph(forest_decomposition, forest_colorings):
    matching = set()
    matched_nodes = set()
    all_colors = get_all_colors(forest_colorings)
    for forest, forest_coloring in zip(forest_decomposition.values(), forest_colorings):
        for color in all_colors:
            for node in [node for node in forest.nodes().keys() if
                         forest_coloring[node] == color and node not in matched_nodes]:
                valid_neighbors = [neighbor for neighbor in forest.neighbors(node) if neighbor not in matched_nodes]
                if valid_neighbors:
                    current_match = (node, valid_neighbors[0])
                    matching.add(current_match)
                    print 'Matched {}'.format(current_match)
            matched_nodes = matched_nodes.union(set(itertools.chain.from_iterable(matching)))
    return matching


def color_edges(forest_decomposition, forest_colorings, max_degree):
    edges_colors = dict()
    all_colors = get_all_colors(forest_colorings)
    for forest, forest_coloring in zip(forest_decomposition.values(), forest_colorings):
        for color in all_colors:
            stars_edge_set = [(u, v) for u, v in forest.edges() if forest_coloring[u] == color]
            for edge in stars_edge_set:
                chosen_color = get_first_available_edge_color(edge, edges_colors, max_degree)
                edges_colors[edge] = chosen_color
                print 'node-{} colored {} with {}'.format(edge[0], edge, chosen_color)
    return edges_colors


def get_max_degree(G):
    return max(G.degree[node] for node in G.nodes().keys())


def get_first_available_vertex_color(coloring, vertex, graph, max_color):
    all_colors = set(xrange(max_color))
    for colored_node, color in coloring.items():
        if colored_node in graph.neighbors(vertex):
            all_colors -= color
    return min(all_colors)


def shrink_coloring(coloring, graph, max_color):
    relevant_colors = [color for color in coloring.values() if color > max_color]
    for color in relevant_colors:
        for v in [vertex for vertex in coloring.keys() if coloring[vertex] == color]:
            coloring[v] = get_first_available_vertex_color(coloring, v, graph, max_color)
    return coloring


def color_nodes(forest_decomposition, forest_colorings, max_degree):
    pass

SHOULD_PLOT = True
G = create_graph()
g_layout = nx.kamada_kawai_layout(G)  # , seed=1337)
plot_graph_to_file(G, r'~\original.jpeg', g_layout)

forest_decomposition = decompose_graph(G)
forest_vertex_coloring = [welsh_color_nodes(subgraph) for subgraph in forest_decomposition.values()]

for (rank, subgraph), coloring in zip(forest_decomposition.items(), forest_vertex_coloring):
    plot_graph_to_file(subgraph,
                       r'~\forest_decomposition\forest{}.jpeg'.format(
                           rank),
                       g_layout)

    plot_graph_to_file(subgraph,
                       r'~\after_coloring\forest{}.jpeg'.format(
                           rank),
                       g_layout,
                       vertex_colors=coloring)

match = match_graph(forest_decomposition, forest_vertex_coloring)

plot_match_graph(G, match,
                 r'~\matched.jpeg',
                 g_layout)


g_max_degree = get_max_degree()
print 'Max degree in graph: {}'.format(g_max_degree)
edge_coloring = color_edges(forest_decomposition, forest_vertex_coloring, g_max_degree)
plot_edge_coloring_graph(G, edge_coloring,
                         r'~\edge_colored.jpeg', g_layout)
